const more1 = document.querySelector(".main__buy-product [data-more-1]");
const less1 = document.querySelector(".main__buy-product [data-less-1]");
const elementQuantity = document.querySelector(".main__buy-product [data-current-quantity]");

more1.addEventListener("click", increaseQuantity);
less1.addEventListener("click", decreaseQuantity);

let valorInitial = '1';
elementQuantity.value = valorInitial;

function decreaseQuantity() {
    let currentQuantity = Number(elementQuantity.value);

    if (currentQuantity <= 1) {
        return;
    }

    --currentQuantity;
    String(currentQuantity).length <= 1
        ? (elementQuantity.value = currentQuantity)
        : (elementQuantity.value = currentQuantity);
}

function increaseQuantity() {
    let currentQuantity = Number(elementQuantity.value);

    if (currentQuantity >= 20) {
      return;
    }

    ++currentQuantity;
    String(currentQuantity).length <= 1
        ? elementQuantity.value = currentQuantity
        : elementQuantity.value = currentQuantity;
}