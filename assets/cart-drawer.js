const cartSelectors = {
    cartDrawer: null,
    overlayCartDrawer: null,
    buttonsOpenToCart: null,
    buttonsRemoveItem: null,
    inputsQuantity: null,
    templateCartDrawer: null,
    formAddProduct: null,
    countCartItems: null,
}

var cartDrawer = {

    getSelectors() {
        cartSelectors.cartDrawer = document.querySelector("[data-js='cart-drawer']");
        cartSelectors.overlayCartDrawer = document.querySelector("#overlay");
        cartSelectors.buttonsOpenToCart = Array.from(document.querySelectorAll("[data-js='open-cart']"));
        cartSelectors.buttonsRemoveItem = Array.from(document.querySelectorAll("[data-remove-item]"));
        cartSelectors.inputsQuantity = Array.from(document.querySelectorAll("[data-quantity-input]"));

        cartSelectors.templateCartDrawer = document.querySelector("[data-js='template-cart-drawer']")
        cartSelectors.formAddProduct = document.querySelector("[data-js='form-add-product']");
        cartSelectors.countCartItems = document.querySelector("[data-cart-count]");
    },

    addEventListener() {

        if(cartSelectors.formAddProduct) {
            cartSelectors.formAddProduct.addEventListener("submit", cartDrawer.addProduct);
        }

        if(cartSelectors.cartDrawer) {

            cartSelectors.buttonsRemoveItem.forEach(btn => {
                btn.addEventListener("click", cartDrawer.removeProductCart);
            })
            
            cartSelectors.buttonsOpenToCart.forEach(btn => {
                btn.addEventListener("click", cartDrawer.handleCartVisible)
            })
            
            cartSelectors.inputsQuantity.forEach(btn => {
                btn.addEventListener("change", (e) => {
                    cartDrawer.modifyQuantityProductCart(e.target);
                })
            })
        }
    },

    resetEventListener() {

        // Resetar Evento nos buttons more e less
        cartDrawer.handleInputQuantity();

        cartSelectors.buttonsRemoveItem.forEach(btn => {
            btn.removeEventListener("click", cartDrawer.removeProductCart);
        })
        
        cartSelectors.buttonsOpenToCart.forEach(btn => {
            btn.removeEventListener("click", cartDrawer.handleCartVisible)
        })

        cartSelectors.inputsQuantity.forEach(btn => {
            btn.removeEventListener("change", (e) => {
                cartDrawer.modifyQuantityProductCart(e.target);
            })
        })
        
        cartSelectors.cartDrawer = document.querySelector("[data-js='cart-drawer']");
        cartSelectors.overlayCartDrawer = document.querySelector("#overlay");
        cartSelectors.buttonsOpenToCart = Array.from(document.querySelectorAll("[data-js='open-cart']"));
        cartSelectors.buttonsRemoveItem = Array.from(document.querySelectorAll("[data-remove-item]"));
        cartSelectors.inputsQuantity = Array.from(document.querySelectorAll("[data-quantity-input]"));
        
        // console.log(cartDrawer, overlayCartDrawer, buttonsRemoveItem, buttonsOpenToCart)

        cartSelectors.buttonsRemoveItem.forEach(btn => {
            btn.addEventListener("click", cartDrawer.removeProductCart);
        })

        cartSelectors.buttonsOpenToCart.forEach(btn => {
            btn.addEventListener("click", cartDrawer.handleCartVisible)
        })

        cartSelectors.inputsQuantity.forEach(btn => {
            btn.addEventListener("change", (e) => {
                cartDrawer.modifyQuantityProductCart(e.target);
            })
        })
    },

    handleCartVisible({ cartToOpen = false }) {
        
        const cartOpen = cartSelectors.cartDrawer.getAttribute("data-cart-open");
    
        if(cartOpen === "true") {
            body.style.overflowY = 'scroll';
            cartSelectors.cartDrawer.setAttribute("data-cart-open", "false");
            cartSelectors.overlayCartDrawer.setAttribute("data-cart-open", "false");
        }
        else if (cartOpen === "false") {
            body.style.overflowY = 'hidden';
            cartSelectors.cartDrawer.setAttribute("data-cart-open", "true");
            cartSelectors.overlayCartDrawer.setAttribute("data-cart-open", "true");
        }
        
        if(cartToOpen) {
            body.style.overflowY = 'hidden';
            cartSelectors.cartDrawer.setAttribute("data-cart-open", "true");
            cartSelectors.overlayCartDrawer.setAttribute("data-cart-open", "true");
        }
    },

    handleInputQuantity() {

        const btnLessOne = Array.from(document.querySelectorAll(".cart-drawer [data-less-1]"));
        const btnMoreOne = Array.from(document.querySelectorAll(".cart-drawer [data-more-1]"));
    
        if(btnMoreOne && btnLessOne) {
    
            btnLessOne.forEach(btn => {
                btn.removeEventListener("click", decreaseQuantity)
                btn.addEventListener("click", decreaseQuantity)
            })
            btnMoreOne.forEach(btn => {
                btn.removeEventListener("click", increaseQuantity)
                btn.addEventListener("click", increaseQuantity)
            })
        }

        function decreaseQuantity(event) {
            const currentElement = event.target;
            const idInputQuantity = currentElement.dataset['less-1'];
            const quantityElement = document.querySelector(`[data-quantity-input='${idInputQuantity}']`);
            let quantityElementValue = quantityElement.value;
            
            if (quantityElementValue <= 1) {
                return;
            }
            
            --quantityElementValue;
            quantityElement.value = "";
            quantityElement.value = quantityElementValue;
            
            cartDrawer.modifyQuantityProductCart(quantityElement);
        }
        
        function increaseQuantity(event) {
            const currentElement = event.target;
            const idInputQuantity = currentElement.dataset['more-1'];
            const quantityElement = document.querySelector(`[data-quantity-input='${idInputQuantity}']`);
            let quantityElementValue = quantityElement.value;
            
            if (quantityElementValue >= 99) {
                return;
            }
            
            ++quantityElementValue;
            quantityElement.value = "";
            quantityElement.value = quantityElementValue;
            
            cartDrawer.modifyQuantityProductCart(quantityElement);
        }
    
    },

    removeProductCart(e) {

        const clickedElement = e.target;
        const variantId = e.target.dataset.removeItem;

        if(variantId) {
            clickedElement.disabled = true;

            const productLI = document.querySelector(`[data-product='${variantId}']`);

            const item = {
                id: variantId,
                quantity: 0
            }
        
            const options = {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(item)
            }

            fetch("/cart/change.js", options)
            .then(response => response.json())
            .then(data => {

                cartDrawer.getItems();
                cartDrawer.uptadeCart()
                    .then(template => {

                        productLI.classList.add("remove-item");

                        setTimeout(function() {
    
                            cartSelectors.templateCartDrawer.innerHTML = template["cart-drawer"];
                            cartDrawer.resetEventListener();
                            console.log("uptadeCart");
                            
                            cartDrawer.handleCartVisible({ cartToOpen: true });
                            console.log("abrir cart-drawer")
                            clickedElement.disabled = false;
                        }, 500)

                    });
            })
            .catch(error => {
                console.error(error);
                clickedElement.disabled = false;
            })
        }
    },

    modifyQuantityProductCart(targetQuantity) {

        const inputModifyValue = targetQuantity;
        const variantId = inputModifyValue.dataset.modifyQuantityItem;
        const quantity = inputModifyValue.value;

        const lineItemId = inputModifyValue.dataset.quantityInput;
        const moreButton = document.querySelector(`[data-more-1='${lineItemId}']`)
        const lessButton = document.querySelector(`[data-less-1='${lineItemId}']`)
        console.log(inputModifyValue, variantId, quantity, moreButton, lessButton)

        if(variantId && quantity) {
            inputModifyValue.disabled = true;
            lessButton.disabled = true;
            moreButton.disabled = true;

            const item = {
                id: variantId,
                quantity: quantity
            }
        
            const options = {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(item)
            }

            fetch("/cart/change.js", options)
            .then(response => response.json())
            .then(data => {

                cartDrawer.getItems();
                cartDrawer.uptadeCart()
                    .then(template => {

                        setTimeout(function() {
                            cartSelectors.templateCartDrawer.innerHTML = template["cart-drawer"];
                            cartDrawer.resetEventListener();
                            console.log("uptadeCart");
                            
                            cartDrawer.handleCartVisible({ cartToOpen: true });
                            console.log("abrir cart-drawer");

                            console.log("disabled off");
                            inputModifyValue.disabled = false;
                            lessButton.disabled = false;
                            moreButton.disabled = false;
                        }, 450)

                    });
            })
            .catch(error => {
                console.log("disabled off");
                inputModifyValue.disabled = false;
                lessButton.disabled = false;
                moreButton.disabled = false;

                console.error(error)
            })
        }
    },

    addProduct(e) {
        
        e.preventDefault();
        
        const buttonBuy = document.querySelector("[data-add-to-cart]");

        buttonBuy.disabled = true;

        const variantId = e.target["variant-id"].getAttribute("data-variant-id");
        let productQuantity = e.target.quantity.value;

        productQuantity[0] === "0" ? productQuantity = productQuantity.replace("0", "") : null

        const item = {
            id: variantId,
            quantity: productQuantity
        }

        const options = {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify(item)
        }

        fetch("/cart/add.js", options)
            .then(response => response.json())
            .then(data => {

                cartDrawer.getItems();
                cartDrawer.uptadeCart()
                    .then(template => {

                        cartSelectors.templateCartDrawer.innerHTML = template["cart-drawer"];
                        cartDrawer.resetEventListener();
                        console.log("uptadeCart");

                        setTimeout(function() {
                            cartDrawer.handleCartVisible({ cartToOpen: true });
                            console.log("abrir cart-drawer")
                            buttonBuy.disabled = false;
                        }, 400)
                    });
        
            })
            .catch(error => {
                buttonBuy.disabled = false;
                console.error(error)
            })

        buttonBuy.disabled = false;
    },

    async getItems() {
        const response = await fetch("/cart.js");
        const data = await response.json();
    
        console.log("Items --> ", data);
        cartSelectors.countCartItems.textContent = data.item_count;
    },

    async uptadeCart() {

        const response = await fetch("/?sections=cart-drawer");
        const template = await response.json();

        return template;
    },

    init() {
        cartDrawer.getSelectors();
        cartDrawer.addEventListener();
        cartDrawer.handleInputQuantity();
    }
}

cartDrawer.init();

// const templateCartDrawer = document.querySelector("[data-js='template-cart-drawer']")
// const formAddProduct = document.querySelector("[data-js='form-add-product']");
// const countCartItems = document.querySelector("[data-cart-count]");

// let cartDrawer = document.querySelector("[data-js='cart-drawer']");
// let overlayCartDrawer = document.querySelector("#overlay");
// let buttonsOpenToCart = Array.from(document.querySelectorAll("[data-js='open-cart']"));
// let buttonsRemoveItem = Array.from(document.querySelectorAll("[data-remove-item]"));
// let inputsQuantity = Array.from(document.querySelectorAll("[data-quantity-input]"));

// if(cartDrawer) {

//     buttonsRemoveItem.forEach(btn => {
//         btn.addEventListener("click", removeProductCart);
//     })
    
//     buttonsOpenToCart.forEach(btn => {
//         btn.addEventListener("click", handleCartVisible)
//     })
    
//     inputsQuantity.forEach(btn => {
//         btn.addEventListener("change", (e) => {
//             modifyQuantityProductCart(e.target);
//         })
//     })
// }

// function removeProductCart(e) {

//     const clickedElement = e.target;
//     const variantId = e.target.dataset.removeItem;

//     if(variantId) {
//         clickedElement.disabled = true;

//         const productLI = document.querySelector(`[data-product='${variantId}']`);

//         const item = {
//             id: variantId,
//             quantity: 0
//         }
    
//         const options = {
//             headers: {
//                 'Accept': 'application/json',
//                 'Content-Type': 'application/json'
//             },
//             method: "POST",
//             body: JSON.stringify(item)
//         }

//         fetch("/cart/change.js", options)
//         .then(response => response.json())
//         .then(data => {

//             getItems();
//             uptadeCart()
//                 .then(template => {

//                     productLI.classList.add("remove-item");

//                     setTimeout(() => {
//                         templateCartDrawer.innerHTML = template["cart-drawer"];
//                         resetEventListener();
//                         console.log("uptadeCart");
                        
//                         handleCartVisible({ cartToOpen: true });
//                         console.log("abrir cart-drawer")
//                         clickedElement.disabled = false;
//                     }, 500)

//                 });
//         })
//         .catch(error => {
//             console.error(error);
//             clickedElement.disabled = false;
//         })
//     }
// }

// function modifyQuantityProductCart(targetQuantity) {

//     const inputModifyValue = targetQuantity;
//     const variantId = inputModifyValue.dataset.modifyQuantityItem;
//     const quantity = inputModifyValue.value;

//     const lineItemId = inputModifyValue.dataset.quantityInput;
//     const moreButton = document.querySelector(`[data-more-1='${lineItemId}']`)
//     const lessButton = document.querySelector(`[data-less-1='${lineItemId}']`)
//     console.log(inputModifyValue, variantId, quantity, moreButton, lessButton)

//     if(variantId && quantity) {
//         inputModifyValue.disabled = true;
//         lessButton.disabled = true;
//         moreButton.disabled = true;

//         const item = {
//             id: variantId,
//             quantity: quantity
//         }
    
//         const options = {
//             headers: {
//                 'Accept': 'application/json',
//                 'Content-Type': 'application/json'
//             },
//             method: "POST",
//             body: JSON.stringify(item)
//         }

//         fetch("/cart/change.js", options)
//         .then(response => response.json())
//         .then(data => {

//             getItems();
//             uptadeCart()
//                 .then(template => {

//                     setTimeout(() => {
//                         templateCartDrawer.innerHTML = template["cart-drawer"];
//                         resetEventListener();
//                         console.log("uptadeCart");
                        
//                         handleCartVisible({ cartToOpen: true });
//                         console.log("abrir cart-drawer");

//                         console.log("disabled off");
//                         inputModifyValue.disabled = false;
//                         lessButton.disabled = false;
//                         moreButton.disabled = false;
//                     }, 450)

//                 });
//         })
//         .catch(error => {
//             console.log("disabled off");
//             inputModifyValue.disabled = false;
//             lessButton.disabled = false;
//             moreButton.disabled = false;

//             console.error(error)
//         })
//     }
// }

// if(formAddProduct) {
//     formAddProduct.addEventListener("submit", addProduct);
// }

// function addProduct(event) {

//     event.preventDefault();
    
//     const buttonBuy = document.querySelector("[data-add-to-cart]");

//     buttonBuy.disabled = true;

//     const variantId = event.target["variant-id"].getAttribute("data-variant-id");
//     let productQuantity = event.target.quantity.value;

//     productQuantity[0] === "0" ? productQuantity = productQuantity.replace("0", "") : null

//     const item = {
//         id: variantId,
//         quantity: productQuantity
//     }

//     const options = {
//         headers: {
//             'Accept': 'application/json',
//             'Content-Type': 'application/json'
//         },
//         method: "POST",
//         body: JSON.stringify(item)
//     }

//     fetch("/cart/add.js", options)
//         .then(response => response.json())
//         .then(data => {

//             getItems();
//             uptadeCart()
//                 .then(template => {

//                     templateCartDrawer.innerHTML = template["cart-drawer"];
//                     resetEventListener();
//                     console.log("uptadeCart");

//                     setTimeout(function() {
//                         handleCartVisible({ cartToOpen: true });
//                         console.log("abrir cart-drawer")
//                         buttonBuy.disabled = false;
//                     }, 400)
//                 });
    
//         })
//         .catch(error => {
//             buttonBuy.disabled = false;
//             console.error(error)
//         })

//     buttonBuy.disabled = false;

// }

// async function getItems() {

//     const response = await fetch("/cart.js");
//     const data = await response.json();

//     console.log("Items --> ", data);
//     countCartItems.textContent = data.item_count;
// }

// async function uptadeCart() {

//     const response = await fetch("/?sections=cart-drawer");
//     const template = await response.json();

//     return template;
// }

// function resetEventListener() {

//     // Resetar Evento nos buttons more e less
//     handleInputQuantity();

//     buttonsRemoveItem.forEach(btn => {
//         btn.removeEventListener("click", removeProductCart);
//     })
    
//     buttonsOpenToCart.forEach(btn => {
//         btn.removeEventListener("click", handleCartVisible)
//     })

//     inputsQuantity.forEach(btn => {
//         btn.removeEventListener("change", (e) => {
//             modifyQuantityProductCart(e.target);
//         })
//     })
    
//     cartDrawer = document.querySelector("[data-js='cart-drawer']");
//     overlayCartDrawer = document.querySelector("#overlay");
//     buttonsOpenToCart = Array.from(document.querySelectorAll("[data-js='open-cart']"));
//     buttonsRemoveItem = Array.from(document.querySelectorAll("[data-remove-item]"));
//     inputsQuantity = Array.from(document.querySelectorAll("[data-quantity-input]"));
    
//     // console.log(cartDrawer, overlayCartDrawer, buttonsRemoveItem, buttonsOpenToCart)

//     buttonsRemoveItem.forEach(btn => {
//         btn.addEventListener("click", removeProductCart);
//     })

//     buttonsOpenToCart.forEach(btn => {
//         btn.addEventListener("click", handleCartVisible)
//     })

//     inputsQuantity.forEach(btn => {
//         btn.addEventListener("change", (e) => {
//             modifyQuantityProductCart(e.target);
//         })
//     })
// }

// function handleCartVisible({ cartToOpen = false }) {

//     const cartOpen = cartDrawer.getAttribute("data-cart-open");
    
//     if(cartOpen === "true") {
//         body.style.overflowY = 'scroll';
//         cartDrawer.setAttribute("data-cart-open", "false");
//         overlayCartDrawer.setAttribute("data-cart-open", "false");
//     }
//     else {
//         body.style.overflowY = 'hidden';
//         cartDrawer.setAttribute("data-cart-open", "true");
//         overlayCartDrawer.setAttribute("data-cart-open", "true");
//     }
    
//     if(cartToOpen) {
//         body.style.overflowY = 'hidden';
//         cartDrawer.setAttribute("data-cart-open", "true");
//         overlayCartDrawer.setAttribute("data-cart-open", "true");
//     }

// }


// // HANDLE INPUT QUANTITY

// function handleInputQuantity() {

//     const btnLessOne = Array.from(document.querySelectorAll(".cart-drawer [data-less-1]"));
//     const btnMoreOne = Array.from(document.querySelectorAll(".cart-drawer [data-more-1]"));

//     if(btnMoreOne && btnLessOne) {

//         btnLessOne.forEach(btn => {
//             btn.removeEventListener("click", decreaseQuantity)
//             btn.addEventListener("click", decreaseQuantity)
//         })
//         btnMoreOne.forEach(btn => {
//             btn.removeEventListener("click", increaseQuantity)
//             btn.addEventListener("click", increaseQuantity)
//         })
//     }

// }

// function decreaseQuantity(event) {
//     const currentElement = event.target;
//     const idInputQuantity = currentElement.dataset['less-1'];
//     const quantityElement = document.querySelector(`[data-quantity-input='${idInputQuantity}']`);
//     let quantityElementValue = quantityElement.value;
    
//     if (quantityElementValue <= 1) {
//         return;
//     }
    
//     --quantityElementValue;
//     quantityElement.value = "";
//     quantityElement.value = quantityElementValue;
    
//     modifyQuantityProductCart(quantityElement);
// }

// function increaseQuantity(event) {
//     const currentElement = event.target;
//     const idInputQuantity = currentElement.dataset['more-1'];
//     const quantityElement = document.querySelector(`[data-quantity-input='${idInputQuantity}']`);
//     let quantityElementValue = quantityElement.value;
    
//     if (quantityElementValue >= 99) {
//         return;
//     }
    
//     ++quantityElementValue;
//     quantityElement.value = "";
//     quantityElement.value = quantityElementValue;
    
//     modifyQuantityProductCart(quantityElement);
// }

// handleInputQuantity();