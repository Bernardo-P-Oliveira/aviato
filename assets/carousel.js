const carouselFirst = $(".banner__carousel-content");

carouselFirst.owlCarousel({
  mouseDrag: true,
  touchDrag: true,
  center: true,
  items: 1,
  loop: true,
  margin: 10,
  nav: false,
  dots: false,
  autoplay: true,
  autoplayTimeout: 10000,
  autoplayHoverPause: true,
  responsive: {
    1000: {
    },
  },
});